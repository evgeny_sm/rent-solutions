/* Правила валидации:
Все валидации происходят после потери фокуса с элемента и при отправке формы.
1. Все поля не могут быть пустыми при отправке. Но если пользователь кликнул по
  полю и не стал вводить значение, то такое поведение не считается ошибкой.
2. Все поля могут cодержать только цифры.
3. Номер карты должен состоять из 16 цифр.
4. Номер месяца и номер года имеют двухзначный формат.
5. Номер месяца должен существовать.
6. Год окончания действия карты не может быть меньше текущего года.
7. Обычно в банке выдают карту на 5 лет. Если ввести год больше, чем срок годности карты, то
  такое поведение тоже считается ошибочным.
8. CVV должен состоять из 3 цифр.
*/

/* Маска для поля номера карты
  Маска для поля будет применяться по потере фокуса. После каждых 4 цифр нужно вставить пробел.
*/

/* Выполнить валидацию и отобразить ошибки */
function validateAndShowError(input, oTooltip, func) {
  const error = func()
  if (!error) return true

  if (input instanceof Array) {
    input.forEach(inp => setError(inp))
  } else {
    setError(input)
  }
  renderTooltip(oTooltip, error.message)
  return false
}

/* Проверка: значение не пустое */
function isEmpty(value) {
  if (!value) return {
    message: 'Поле не может быть пустым'
  }
}

/* Проверка: фиксированная длина значения */
function fixedLen(value, len, text) {
  if (value.length !== len) return {
    message: text
  }
}

/* Проверка: наличие символов кроме цифр */
function onlyDigit(value) {
  const digits = value.match(/\d/g) || []
  if (digits.join('') !== value) return { message: 'Поле может содержать только цифры' }
}

/* Проверка: значение X меньше значения Y*/
function lessThan(value, maxValue, text) {
  if (value > maxValue) return {message: text}
}

/* Валидация номера карты */
function cardNumberValidate(cardNumberValue) {
  cardNumberValue = cardNumberValue.replace(/ /gi, '')
  return isEmpty(cardNumberValue)
    || fixedLen(cardNumberValue, 16, 'Номер карты должен состоять из 16 цифр')
    || onlyDigit(cardNumberValue)
}

/* Валидация срока действия карты */
function validateDate(cardMonthValue, cardYearValue) {
  const currentYear = new Date().getFullYear() - 2000
  const currentMonth = new Date().getMonth() + 1

  if (Number(cardYearValue) === currentYear && cardMonthValue < currentMonth) return {
    message: 'Срок действия карты истек'
  }
}

function cardDateValidation(cardDateValue) {
  cardDateValue = cardDateValue.replace('/', '')
  const [cardMonthValue, cardYearValue] = cardDateValue.match(/\d{1,2}/gi)

  const currentYear = new Date().getFullYear() - 2000
  return onlyDigit(cardDateValue)
    || fixedLen(cardDateValue, 4, 'Срок действия карты - 4 символа')
    || lessThan(cardMonthValue, 12, 'Некорректный номер месяца')
    || lessThan(currentYear, +cardYearValue, 'Срок действия карты истек')
    || lessThan(cardYearValue - currentYear, 6, 'Максимальный срок действия карты&nbsp;&mdash;&nbsp;5&nbsp;лет')
    || validateDate(cardMonthValue, cardYearValue)
}

/* Валидация CVV кода */
function CVVValidate(cardCvvValue) {
  return isEmpty(cardCvvValue)
    || fixedLen(cardCvvValue, 3, 'CVV должен состоять из трех цифр')
    || onlyDigit(cardCvvValue)
}

/* Установить класс ошибки на поле ввода */
function setError(elem) {
  elem.classList.add('_error')
}

/* Убрать класс ошибки с поля ввода */
function removeError(elem) {
  elem.classList.remove('_error')
}

/* Отобразить Тултип */
function renderTooltip(tooltip, error) {
  if (!tooltip) throw Error('Что-то пошло не так')

  tooltip.innerHTML = error
  tooltip.classList.add('_visible')
  tooltip.classList.remove('_hidden')
}

/* Убрать тултип */
function removeTooltip(tooltip) {
  if (!tooltip) throw Error('Что-то пошло не так')

  tooltip.innerHTML = ''
  tooltip.classList.remove('_visible')
  tooltip.classList.add('_hidden')
}

/* Установить обратчик скрывающий ошибки по фокусу на элементе */
function removeErrorOnFocus(elem, tooltip) {
  elem.addEventListener('focus', function () {
    removeTooltip(tooltip)
    removeError(elem)
  })
}

/* Установить обработчик выполняющий валидацию по потере фокуса */
function setBlurValidation(oInput, oTooltip, func) {
  oInput.addEventListener('blur', () => {
    if (!oInput.value) return
    const err = func(oInput.value)
    if (err) {
      setError(oInput)
      renderTooltip(oTooltip, err.message)
    }
  })
}

document.addEventListener('DOMContentLoaded', function () {
  const cardNumber = document.getElementById('card-number')
  const cardDate = document.getElementById('card-date')
  const cardCVV = document.getElementById('card-cvv')
  const form = document.getElementById('form')

  const cardNumberTip = document.querySelector('[data-id=card-number]')
  const cardCVVTip = document.querySelector('[data-id=card-cvv]')
  const cardDateTip = document.querySelector('[data-id=card-date]')

  removeErrorOnFocus(cardNumber, cardNumberTip)
  removeErrorOnFocus(cardDate, cardDateTip)
  removeErrorOnFocus(cardCVV, cardCVVTip)

  setBlurValidation(cardNumber, cardNumberTip, cardNumberValidate)
  setBlurValidation(cardDate, cardDateTip, cardDateValidation)
  setBlurValidation(cardCVV, cardCVVTip, CVVValidate)

  cardNumber.addEventListener('change', () => {
    const val = cardNumber.value.replace(/ /gi, '')
    const blocks = val.match(/(.){1,4}/gi) || []
    cardNumber.value = blocks.join(' ')
  })
  cardDate.addEventListener('change', () => {
    const val = cardDate.value.replace(/\//gi, '')
    const blocks = val.match(/\d{1,2}/gi) || []
    cardDate.value = blocks.slice(0, 2).join('/')
  })

  form.addEventListener('submit', e => {
    e.preventDefault()

    let isValid = true
    isValid = isValid & validateAndShowError(cardNumber, cardNumberTip, () => cardNumberValidate(cardNumber.value))
    isValid = isValid & validateAndShowError(cardDate, cardDateTip, () => cardDateValidation(cardDate.value))
    isValid = isValid & validateAndShowError(cardCVV, cardCVVTip, () => CVVValidate(cardCVV.value))

    console.log(isValid ? 'Форма валидна!' : 'Форма заполнена с ошибками!')
  })
})